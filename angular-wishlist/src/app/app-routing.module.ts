import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DestinoDetalleComponent } from '@components/destino-detalle/destino-detalle.component';
import { ListaDestinosComponent } from '@components/lista-destinos/lista-destinos.component';
import { LoginComponent } from '@components/login/login/login.component';
import { ProtectedComponent } from '@components/protected/protected/protected.component';
import { VuelosComponentComponent } from '@components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosDetalleComponentComponent } from '@components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { VuelosMainComponentComponent } from '@components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from '@components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { UsuarioLogueadoGuard } from '@guards/usuario-logueado/usuario-logueado.guard';

export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponentComponent },
  { path: 'mas-info', component: VuelosMasInfoComponentComponent },
  { path: ':id', component: VuelosDetalleComponentComponent },
];

// init routing
export const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinosComponent },
  { path: 'destino/:id', component: DestinoDetalleComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [UsuarioLogueadoGuard],
  },
  {
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate: [UsuarioLogueadoGuard],
    children: childrenRoutesVuelos,
  },
];
// fin init routing

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
