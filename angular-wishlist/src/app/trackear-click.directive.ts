import { Directive, ElementRef } from '@angular/core';
import { TrackearTagsApiService } from '@services/trackear-tags-api.service';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
  private element: HTMLInputElement;

  constructor(private elRef: ElementRef, private trackearTagsApiService: TrackearTagsApiService) {
    this.element = elRef.nativeElement;
    fromEvent(this.element, 'click').subscribe(evento => this.track(evento))
  }

  track(evento: Event): void {
    const elemTags = this.element.attributes.getNamedItem('data-trackear-tags');
    if (elemTags) {
      const tags = elemTags.value.split(' ');
      this.trackearTagsApiService.add(tags);
    }
  }

}
