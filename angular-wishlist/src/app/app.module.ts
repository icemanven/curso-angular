import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { AppLoadService, ini_app } from './core/api.ini';
import { VuelosMainComponentComponent } from '@components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from '@components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from '@components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ListaDestinosComponent } from '@components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from '@components/destino-detalle/destino-detalle.component';
import { LoginComponent } from '@components/login/login/login.component';
import { ProtectedComponent } from '@components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from '@guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponentComponent } from '@components/vuelos/vuelos-component/vuelos-component.component';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from '@components/destino-viaje/destino-viaje.component';
import { FormDestinoViajeComponent } from '@components/form-destino-viaje/form-destino-viaje.component';
import {
  APP_CONFIG,
  APP_CONFIG_VALUE,
  HttpLoaderFactory,
  MyDatabase,
  reducers,
  reducersInitialState,
} from '@core*';
import { AuthService } from '@services/auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { StoreModule as NgRxStoreModule } from '@ngrx/store';
import { DestinosViajeEffects } from '@models/destinos-viajes-state.model';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ReservasModule } from '@reservas/reservas.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';
import { TrackearTagsApiService } from '@services/trackear-tags-api.service';

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackearClickDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgRxStoreModule.forRoot(reducers, {
      initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
        strictStateSerializability: false,
        strictActionSerializability: false,
      },
    }),
    EffectsModule.forRoot([DestinosViajeEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NgxMapboxGLModule,
  ],
  providers: [
    AuthService,
    UsuarioLogueadoGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    {
      provide: APP_INITIALIZER,
      useFactory: ini_app,
      deps: [AppLoadService],
      multi: true,
    },
    MyDatabase,
    TrackearTagsApiService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
