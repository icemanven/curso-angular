import { DestinoViaje } from './destino-viaje.model';
import {
  BorrarDestinoAction,
  DestinosViajesState,
  ElegidoFavoritoAction,
  initializeDestinosViajesState,
  InitMyDataAction,
  NuevoDestinoAction,
  reducerDestinosViajes,
  UpdateTrackingTagsAction,
  VoteDownAction,
  VoteResetAction,
  VoteUpAction,
} from './destinos-viajes-state.model';

describe('ReducerDestinosViajes', () => {
  it('should reduce init data', () => {
    // setup
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2',]);
    // action
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    // assert
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('destino 1');
    // tear down
    // delete data used for testing
  });

  it('should reduce new item added', () => {
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Barcelona', 'url'));
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('Barcelona');
  });

  it('should reduce favorito', () => {
    // setup
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: ElegidoFavoritoAction = new ElegidoFavoritoAction(new DestinoViaje('Barcelona', 'url'));
    // action
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    // assert
    expect(newState.favorito).not.toBe(null);
    expect(newState.favorito!.nombre).toEqual('Barcelona');
  });

  it('should reduce delete item', () => {
    // setup
    let prevState: DestinosViajesState = initializeDestinosViajesState();
    const barcelona: DestinoViaje = new DestinoViaje('Barcelona', 'url')
    const action1: NuevoDestinoAction = new NuevoDestinoAction(barcelona);
    const action2: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Cartagena', 'url'));
    const action3: BorrarDestinoAction = new BorrarDestinoAction(barcelona);
    // action
    prevState = reducerDestinosViajes(prevState, action1);
    prevState = reducerDestinosViajes(prevState, action2);

    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action3);
    // assert
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('Cartagena');
  });

  it('should reduce vote up', () => {
    // setup
    let prevState: DestinosViajesState = initializeDestinosViajesState();
    const barcelona: DestinoViaje = new DestinoViaje('Barcelona', 'url')
    const action1: NuevoDestinoAction = new NuevoDestinoAction(barcelona);
    const action2: VoteUpAction = new VoteUpAction(barcelona);
    // action
    prevState = reducerDestinosViajes(prevState, action1);

    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action2);
    // assert
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('Barcelona');
    expect(newState.items[0].votes).toEqual(1);
  });

  it('should reduce vote down', () => {
    // setup
    let prevState: DestinosViajesState = initializeDestinosViajesState();
    const barcelona: DestinoViaje = new DestinoViaje('Barcelona', 'url')
    const action1: NuevoDestinoAction = new NuevoDestinoAction(barcelona);
    const action2: VoteUpAction = new VoteUpAction(barcelona);
    const action3: VoteDownAction = new VoteDownAction(barcelona);
    // action
    prevState = reducerDestinosViajes(prevState, action1);
    prevState = reducerDestinosViajes(prevState, action2);

    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action3);
    // assert
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('Barcelona');
    expect(newState.items[0].votes).toEqual(0);
  });

  it('should reduce vote reset', () => {
    // setup
    let prevState: DestinosViajesState = initializeDestinosViajesState();
    const barcelona: DestinoViaje = new DestinoViaje('Barcelona', 'url')
    const action1: NuevoDestinoAction = new NuevoDestinoAction(barcelona);
    const action2: VoteUpAction = new VoteUpAction(barcelona);
    const action3: VoteResetAction = new VoteResetAction();
    // action
    prevState = reducerDestinosViajes(prevState, action1);
    prevState = reducerDestinosViajes(prevState, action2);

    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action3);
    // assert
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('Barcelona');
    expect(newState.items[0].votes).toEqual(0);
  });

  it('should reduce update tracking tags', () => {
    // setup
    let prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: UpdateTrackingTagsAction = new UpdateTrackingTagsAction(['tag1', 'tag2'],);
    // action
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    // assert
    expect(newState.trackingTags.length).toEqual(2);
    expect(newState.trackingTags[1]).toEqual('tag2');
  });
});
