import { forwardRef, Inject, Injectable } from "@angular/core";
import { DestinoViaje } from "./destino-viaje.model";
import { Store } from "@ngrx/store";
import { ElegidoFavoritoAction, NuevoDestinoAction, VoteResetAction } from "./destinos-viajes-state.model";
import { AppConfig, AppState, APP_CONFIG, db } from "@core*";
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from "@angular/common/http";

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];

  constructor(
    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
  ) {
    this.store
    .select(state => state.destinos)
    .subscribe(data => {
      console.log('destinos sub store');
      console.log(data);
      this.destinos = data.items;
    });
    this.store
    .subscribe(data => {
      console.log('all store');
      console.log(data);
    });
  }


  add(d: DestinoViaje) {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', {nuevo: d.nombre}, {headers: headers});
    this.http.request(req).subscribe((data) => {
      if ((data as HttpResponse<{}>).status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDB = db;
        myDB.destinos.add(d);
        console.log('todos los destinos de la db!');
        myDB.destinos.toArray().then((destinos: any) => console.log(destinos));
      }
    });
  }

  elegir(e: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  resetVotes() {
    this.store.dispatch(new VoteResetAction());
  }

  getById(id: string | null): DestinoViaje {
    return this.destinos.filter(d => d.id.toString() === id)[0];
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

}

