import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from '../models/destino-viaje.model';

// ESTADO
export interface DestinosViajesState {
  items: DestinoViaje[];
  loading: boolean;
  favorito: DestinoViaje | null;
  trackingTags: string[];
}

export function initializeDestinosViajesState(): DestinosViajesState {
  return {
    items: [],
    loading: false,
    favorito: null,
    trackingTags: []
  };
}

//ACCIONES
export enum DestinosViajesActionTypes {
  NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
  ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
  BORRAR_DESTINO = '[Destinos Viajes] Borrar',
  VOTE_UP = '[Destinos Viajes] Vote Up',
  VOTE_DOWN = '[Destinos Viajes] Vote Down',
  VOTE_RESET = '[Destinos Viajes] Vote Reset',
  INIT_MY_DATA = '[Destinos Viajes] Init My Data',
  UPDATE_TRACKING_TAGS = '[Destinos Viajes] Update Tracking Tags',
}

export class NuevoDestinoAction implements Action {
  type = DestinosViajesActionTypes.NUEVO_DESTINO;
  constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
  type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
  constructor(public destino: DestinoViaje) {}
}

export class BorrarDestinoAction implements Action {
  type = DestinosViajesActionTypes.BORRAR_DESTINO;
  constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
  type = DestinosViajesActionTypes.VOTE_UP;
  constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
  type = DestinosViajesActionTypes.VOTE_DOWN;
  constructor(public destino: DestinoViaje) {}
}

export class VoteResetAction implements Action {
  type = DestinosViajesActionTypes.VOTE_RESET;
  constructor() {}
}

export class InitMyDataAction implements Action {
  type = DestinosViajesActionTypes.INIT_MY_DATA;
  constructor(public destinos: string[]) {}
}

export class UpdateTrackingTagsAction implements Action {
  type = DestinosViajesActionTypes.UPDATE_TRACKING_TAGS;
  constructor(public tags: string[]) {}
}

export type DestinosViajeActions =
  | NuevoDestinoAction
  | ElegidoFavoritoAction
  | BorrarDestinoAction
  | VoteUpAction
  | VoteDownAction
  | VoteResetAction
  | InitMyDataAction
  | UpdateTrackingTagsAction;

//REDUCERS
export function reducerDestinosViajes(
  state: DestinosViajesState,
  action: DestinosViajeActions
): DestinosViajesState {
  switch (action.type) {
    case DestinosViajesActionTypes.INIT_MY_DATA: {
      const destinos: string[] = (action as InitMyDataAction).destinos;
      return {
        ...state,
        items: destinos.map(d => new DestinoViaje(d, ''))
      };
    }
    case DestinosViajesActionTypes.NUEVO_DESTINO: {
      return {
        ...state,
        items: [...state.items, (action as NuevoDestinoAction).destino],
      };
    }
    case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
      state.items.forEach((x) => x.setSelected(false));
      let fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
      fav.setSelected(true);
      return {
        ...state,
        favorito: fav,
      };
    }
    case DestinosViajesActionTypes.BORRAR_DESTINO: {
      const d: DestinoViaje = (action as ElegidoFavoritoAction).destino;
      const items = state.items.filter((i) => i.id != d.id);
      if (d.isSelected() && items.length > 0) {
        items[items.length - 1].setSelected(true);
      }
      return {
        ...state,
        items: [...items],
      };
    }
    case DestinosViajesActionTypes.VOTE_UP: {
      const d: DestinoViaje = (action as VoteUpAction).destino;
      d.voteUp();
      return { ...state };
    }
    case DestinosViajesActionTypes.VOTE_DOWN: {
      const d: DestinoViaje = (action as VoteDownAction).destino;
      d.voteDown();
      return { ...state };
    }
    case DestinosViajesActionTypes.VOTE_RESET: {
      state.items.forEach((x) => x.voteReset());
      return { ...state };
    }
    case DestinosViajesActionTypes.UPDATE_TRACKING_TAGS: {
      const tabs: string[] = (action as UpdateTrackingTagsAction).tags;
      return {
        ...state,
        trackingTags: [...state.trackingTags, ...tabs.filter(t => state.trackingTags.indexOf(t) === -1)]
      };
    }
  }
  return state;
}

//EFFECTS
@Injectable()
export class DestinosViajeEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
    map(
      (action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino)
    )
  );

  constructor(private actions$: Actions) {}
}
