import {v4 as uuid} from 'uuid';
export class DestinoViaje {
  private selected: boolean;
  public servicios: string[];
  id: any;
  votes: number;

  constructor(public nombre:string,	public imagenUrl:string) {
    this.servicios = ['pileta', 'desayuno'];
    this.selected = false;
    this.id = uuid();
    this.votes = 0;
  }

  isSelected() {
    return this.selected;
  }

  setSelected(s: boolean) {
    this.selected = s;
  }

  voteUp() {
    this.votes++;
  }

  voteDown() {
    if (this.votes > 0) {
      this.votes--;
    }
  }

  voteReset() {
    this.votes = 0;
  }
}

