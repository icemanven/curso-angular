import { DestinosViajesState, initializeDestinosViajesState, reducerDestinosViajes } from "@models/destinos-viajes-state.model";
import { ActionReducerMap } from "@ngrx/store";

// redux init
export interface AppState {
  destinos: DestinosViajesState;
}

export const reducers: ActionReducerMap<AppState> | any = {
  destinos: reducerDestinosViajes,
};

export let reducersInitialState = {
  destinos: initializeDestinosViajesState(),
};
// redux fin init
