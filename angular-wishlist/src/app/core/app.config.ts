import { InjectionToken } from "@angular/core";

// app config
export interface AppConfig {
  apiEndPoint: string;
}

export const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'http://localhost:3000'
}

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// fin app config
