import { HttpClient } from "@angular/common/http";
import { from, Observable } from "rxjs";
import { flatMap } from "rxjs/operators";
import { APP_CONFIG_VALUE } from "./app.config";
import { db, Translation } from "./dixie.db";

// i18n ini
class TranslationLoader implements TranslationLoader {
  constructor(private http: HttpClient) {}

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                        if (results.length === 0) {
                          return this.http
                                  .get<Translation[]>(APP_CONFIG_VALUE.apiEndPoint + '/api/translation?lang=' + lang)
                                  .toPromise()
                                  .then(apiResults => {
                                    db.translations.bulkAdd(apiResults);
                                    return apiResults;
                                  });
                        }
                        return results;
                      }).then(traducciones => {
                        console.log('traducciones cargadas');
                        console.log(traducciones);
                        return traducciones;
                      }).then(traducciones => {
                        return traducciones.map(t => ({[t.key] : t.value}));
                      });
    return from(promise).pipe(flatMap(elems => from(elems)));
  }
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
// fin i18n
