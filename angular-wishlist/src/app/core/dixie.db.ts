import { Injectable } from "@angular/core";
import { DestinoViaje } from "@models/destino-viaje.model";
import Dexie from "dexie";

// dexie db
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable()
export class MyDatabase extends Dexie {
  destinos!: Dexie.Table<DestinoViaje, number>;
  translations!: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDatabase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl',
    });
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value'
    });
  }
}

export const db = new MyDatabase();
// fin dexie db
