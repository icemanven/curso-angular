import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { InitMyDataAction } from "@models/destinos-viajes-state.model";
import { Store } from "@ngrx/store";
import { APP_CONFIG_VALUE } from "./app.config";
import { AppState } from "./redux";

// app init
export function ini_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeDestinosViajesState();
}
@Injectable()
export class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) {}

  async initializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({
      'X-API-TOKEN': 'token-seguridad',
    });
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/my', {
      headers: headers,
    });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
// fin app init
