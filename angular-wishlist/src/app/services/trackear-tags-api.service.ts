import { Injectable } from '@angular/core';
import { AppState } from '@core*';
import { UpdateTrackingTagsAction } from '@models/destinos-viajes-state.model';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})
export class TrackearTagsApiService {

  constructor(private store: Store<AppState>,) {

  }

  add(tags: any) {
    console.log(`|||||||||| track evento: "${tags}"`);
    this.store.dispatch(new UpdateTrackingTagsAction(tags));
  }
}
