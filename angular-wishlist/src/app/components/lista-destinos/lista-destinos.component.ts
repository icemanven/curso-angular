import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AppState } from '@core*';
import { DestinoViaje } from '@models/destino-viaje.model';
import { DestinosApiClient } from '@models/destinos-api-client.model';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all: any;

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      if(d != null) {
        this.updates.push('se ha elegido a ' + d.nombre);
      }
    });
    this.store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
    // on init
  }

  agregado(d: DestinoViaje): void	{
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e: DestinoViaje): void {
    this.destinosApiClient.elegir(e);
  }

  resetVotes(): boolean {
    this.destinosApiClient.resetVotes();
    return false;
  }

}
