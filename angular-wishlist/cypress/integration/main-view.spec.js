describe('ventana principal', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200')
  })

  it('tiene encabezado correcto y en espanol por defecto', () => {
    cy.contains('angular-wishlist');
    cy.get('h1 strong').should('contain', 'HOLA es');
  })

  it('tiene formulario wishlist', () => {
    cy.get('.card-header .mb-0').should('contain', 'Whishlist');
    cy.get('.card-body form');
  })

  it('tiene campos nombre en el formulario wishlist', () => {
    cy.get('.card-header .mb-0').should('contain', 'Whishlist');
    cy.get('.card-body form .form-group .form-control').should('have.attr', 'formControlName', 'nombre');
  });

  it('validar foco en el campo url del formulario', () => {
    cy.get('#imagenUrl').click()
    cy.get('#imagenUrl').should('have.focus')
  });
});
